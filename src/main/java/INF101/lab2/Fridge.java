package INF101.lab2;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

public class Fridge implements IFridge {

    private int items = 0;
    List<FridgeItem> list=new ArrayList<FridgeItem>();

    @Override
    public int nItemsInFridge() {
        return items;
    }

    @Override
    public int totalSize() {
        return 20;
    }

    @Override
    public boolean placeIn(FridgeItem item) {
        if (nItemsInFridge() < totalSize()) {
            items ++;
            list.add(item);
            return true;
        }   return false;
    }

    @Override
    public void takeOut(FridgeItem item) {
        if (nItemsInFridge() == 0) {
            throw new NoSuchElementException();
        } else {
            items --;
            list.remove(item);
        }
    }

    @Override
    public void emptyFridge() {
        items = 0;
    }

    @Override
    public List<FridgeItem> removeExpiredFood() {
        List<FridgeItem> expList=new ArrayList<FridgeItem>();
        int count = 0;
        for (int i = 0; i < list.size(); i++) {
            FridgeItem currentItem = list.get(i);
            if (currentItem.hasExpired()) {
                expList.add(currentItem);
                count += 1;
            }
        }
        items -= count;
        return expList;
    }
}
